# encoding: utf-8

from booleens import *
from arbre import *
from compression import *
from dessin import *
from test import *
from synthese import *
from random import seed
seed()

print( "1 - Compression d'un arbre à partir d'un nombre" )
print( "2 - Synthèse à partir de 2 nombres" )
print( "3 - Tests automatiques de la synthèse d'arbres" )

choix = int( input() )

if choix == 1:
    nombreTable = int( input( "Entrez le nombre identifiant la table de vérité : " ) )
    tailleTable = int( input( "Entrez la taille de la table de vérité : " ) )
    arbre = compression( cons_arbre( table( nombreTable, tailleTable ) ) )
    dot( arbre, "arbre.dot" )
    print( "L'arbre se trouve dans le fichier arbre.dot" )

if choix == 2:
    nombreTable1 = int( input( "Entrez le premier identifiant de la table de vérité : " ) )
    tailleTable1 = int( input( "Entrez le taille de la première table de vérité : " ) )
    nombreTable2 = int( input( "Entrez le second identifiant de la table de vérité : " ) )
    tailleTable2 = int( input( "Entrez le taille de la seconde table de vérité : " ) )
    arbre1 = compression_bdd( cons_arbre( table( nombreTable1, tailleTable1 ) ) ) 
    arbre2 = compression_bdd( cons_arbre( table( nombreTable2, tailleTable2 ) ) )
    operateur = eval( input( "Entrez l'opérateur : " ) )
    arbre = synthese( arbre1, arbre2, operateur )
    dot( arbre, "arbre.dot" )
    print( "L'arbre se trouve dans le fichier arbre.dot" )

if choix == 3:
    n_variables = int( input( "Entrez le nombre de variables : " ) )
    operateur = eval( input( "Entrez l'opérateur : " ) )
    n_echantillons = int( input( "Entrez le nombre d'échantillons : " ) )
    tests( n_variables, operateur, n_echantillons )
    print( "Les tests ont réussi" )