from arbre import Arbre, estFeuille
from compression import compression_bdd

#5.18
# Retour négatif si cle1 plus petit, sinon positif.
def compare( cle1, cle2 ):
    if cle1 in [ True, False ] and cle2 in [ True, False ]:
        return 0
    if cle1 in [ True, False ]:
        return -1
    if cle2 in [ True, False ]:
        return 1
    return int( cle1[ 1: ] ) - int( cle2[ 1: ] )

def ArbreAvecEtiquette( cle, false, true, etiquette ):
    arbre = Arbre( cle, false, true )
    arbre.etiquette = etiquette
    return arbre

# Multiplie l'étiquette suivant la distance à la racine
def multiplierEtiquette( cle_racine, fils ):
    etage_racine = int( cle_racine[ 1:] )
    etage_fils = 0 if estFeuille( fils ) else int( fils.cle[ 1: ] )
    return fils.etiquette * int( 2 ** ( etage_racine - etage_fils - 1 ) )

# fusion avec mémoïsation si les arbres furent déjà fusionnés
def fusion( arbre1, arbre2, operateur, fusions ):
    id_fusion =  ( id( arbre1 ), id( arbre2 ) )
    if not id_fusion in fusions:
        if estFeuille( arbre1 ) and estFeuille( arbre2 ):
            fusions[ id_fusion ] = ArbreAvecEtiquette( operateur( arbre1.cle, arbre2.cle ), None, None, "1" if operateur( arbre1.cle, arbre2.cle ) else "0" )
        elif compare( arbre1.cle, arbre2.cle ) > 0: # a1.cle plus grand
            false = fusion( arbre1.false, arbre2, operateur, fusions )
            true = fusion( arbre1.true, arbre2, operateur, fusions )
            fusions[ id_fusion ] = ArbreAvecEtiquette( arbre1.cle, false, true,
                multiplierEtiquette( arbre1.cle, false ) + multiplierEtiquette( arbre1.cle, true ) )
        elif compare( arbre1.cle, arbre2.cle ) < 0: # a1.cle plus petit
            false = fusion( arbre1, arbre2.false, operateur, fusions )
            true = fusion( arbre1, arbre2.true, operateur, fusions )
            fusions[ id_fusion ] = ArbreAvecEtiquette( arbre2.cle, false, true,
                multiplierEtiquette( arbre2.cle, false ) + multiplierEtiquette( arbre2.cle, true ) )
        else:
            false = fusion( arbre1.false, arbre2.false, operateur, fusions )
            true = fusion( arbre1.true, arbre2.true, operateur, fusions )
            fusions[ id_fusion ] = ArbreAvecEtiquette( arbre1.cle, false, true,
                multiplierEtiquette( arbre1.cle, false ) + multiplierEtiquette( arbre1.cle, true ) )
    return fusions[ id_fusion ]

def synthese( arbre1, arbre2, operateur ):
    return compression_bdd( fusion( arbre1, arbre2, operateur, dict() ) )