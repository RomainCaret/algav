# encoding: utf-8

from math import log

# 2.5
class Arbre:
    def __init__( self, cle, false, true ):
        self.cle = cle
        self.false = false
        self.true = true

def estFeuille( arbre ):
    return arbre.false == None and arbre.true == None

# 2.6
def cons_arbre( table_verite ):
    taille = len( table_verite )
    if taille == 1:
        return Arbre( table_verite[ 0 ], None, None )
    else:
        return Arbre(
            "x" + str( int( log( taille, 2 ) ) ),
            cons_arbre( table_verite[ : taille // 2 ] ),
            cons_arbre( table_verite[ taille // 2 : ])
        )

# 2.7
# Étiquette l'arbre avec les mots de Lukasiewicz. Ne retourne rien.
def luka( arbre ):
    if estFeuille( arbre ):
        arbre.etiquette = str( arbre.cle )
    else:
        luka( arbre.false )
        luka( arbre.true )
        arbre.etiquette = str ( arbre.cle ) + "(" +  arbre.false.etiquette + ")(" + arbre.true.etiquette + ")"