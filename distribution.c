#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TAILLE_DISTRIBUTION 100

typedef struct arbre {
    struct arbre * false;
    struct arbre * true;
} arbre;

arbre * nouvelArbre( arbre * false, arbre * true ) {
    arbre * a = malloc( sizeof( arbre ) );
    a -> false = false;
    a -> true = true;
    return a;
}

arbre ** tousArbres( int n_variables ) {

    arbre ** nouveauxArbres = malloc( sizeof( arbre ) * pow( 2, pow( 2, n_variables ) ) );
    if ( n_variables == 0 ) {
        nouveauxArbres[ 0 ] = nouvelArbre( NULL, NULL ); // Feuille false
        nouveauxArbres[ 1 ] = nouvelArbre( NULL, NULL ); // Feuile true
    } else {
        arbre ** anciensArbres = tousArbres( n_variables - 1 );
        int nombreAnciensArbres = pow( 2, pow( 2, n_variables - 1 ) );
        for ( int i1 = 0; i1 < nombreAnciensArbres; i1++ )
        for ( int i2 = 0; i2 < nombreAnciensArbres; i2++ )
            if ( i1 == i2 )
                nouveauxArbres[ i1 * nombreAnciensArbres + i2 ] = anciensArbres[ i1 ];
            else
                nouveauxArbres[ i1 * nombreAnciensArbres + i2 ] = nouvelArbre( anciensArbres[ i1 ], anciensArbres[ i2 ] );        
        free( anciensArbres );
    }
    return nouveauxArbres;
}

int dans( arbre * a, arbre ** rencontres ) {
    
    for ( int i = 0; i < TAILLE_DISTRIBUTION; i++ )
        if ( rencontres[ i ] == a )
            return 1;
        else if ( rencontres[ i ] == NULL )
            return 0;
    return 0;
}

int nouveauNoeud( arbre * noeud, arbre ** rencontres ) {
    for ( int i = 0; i < TAILLE_DISTRIBUTION; i++ )
        if ( rencontres[ i ] == NULL ) {
            rencontres[ i ] = noeud;
            return 1;
        }
        else if ( rencontres[ i ] == noeud )
            return 0;
}

int compterNoeudsRec( arbre * a, arbre ** rencontres ) {
    if ( a == NULL )
        return 0;
    if ( nouveauNoeud( a, rencontres ) ) {
        return 1 + compterNoeudsRec( a -> false, rencontres ) + compterNoeudsRec( a -> true, rencontres );
    } else
        return 0;
}

int compterNoeuds( arbre * a ) {
    arbre * rencontres[ TAILLE_DISTRIBUTION ];
    for ( int i = 0; i < TAILLE_DISTRIBUTION; i++ )
        rencontres[ i ] = NULL;
    int compte = compterNoeudsRec( a, rencontres );
    return compte;
}

void main( int argc, char * argv[] ) {
    
    int n_variables = 5;
    arbre ** anciensArbres = tousArbres( n_variables - 1 );
    long int nombreAnciensArbres = pow( 2, pow( 2, n_variables - 1 ) );

    int distribution[ TAILLE_DISTRIBUTION ];
    for ( int i = 0; i < TAILLE_DISTRIBUTION; i++ )
        distribution[ i ] = 0;
    
    for ( int i1 = 0; i1 < nombreAnciensArbres; i1++ ) {
    for ( int i2 = 0; i2 < nombreAnciensArbres; i2++ )
        if ( i1 == i2 )
            distribution[ compterNoeuds( anciensArbres[ i1 ] ) ] += 1;
        else {
            arbre a = { anciensArbres[ i1 ], anciensArbres[ i2 ] };
            distribution[ compterNoeuds( & a ) ] += 1;
        }
        printf( "%f\n", ( double ) i1 / nombreAnciensArbres );
    }
    free( anciensArbres );
    for ( int i = 0; i < TAILLE_DISTRIBUTION; i++ )
        if ( distribution[ i ] != 0 )
            printf( "%d : %d\n", i, distribution[ i ] );
}