# encoding: utf-8

from arbre import estFeuille

# 2.9
def dot( arbre, nom_de_fichier ):
    fichier = open( nom_de_fichier, "w" )
    fichier.write( "graph {\n" )
    dot_rec( arbre, fichier )
    fichier.write( "}" )
    fichier.close

def dot_rec( arbre, fichier, dejaDessines = [] ):
    if not arbre in dejaDessines:
        dejaDessines.append( arbre )
        fichier.write( str( id( arbre ) ) )
        fichier.write( "[ label = " + str( arbre.cle ) + " ]\n" )
        if not estFeuille( arbre ):
            fichier.write( str( id( arbre ) ) + " -- " + str( id( arbre.false ) ) + " [ style = dotted ]\n" )
            fichier.write( str( id( arbre ) ) + " -- " + str( id( arbre.true ) ) + "\n" )
            dot_rec( arbre.false, fichier, dejaDessines )
            dot_rec( arbre.true, fichier, dejaDessines )