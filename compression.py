# encoding: utf-8

# 2.8
from arbre import estFeuille, luka


def compression( arbre ):
    if not hasattr( arbre, "etiquette" ):
        luka( arbre )
    #noeudsRencontres est un arbre lexicographique dont les chemins forment des mots de Lukasiewicz. Il pointe vers le premier nœud ayant la structure du mot.
    noeudsRencontres = dict()
    return compression_rec( arbre, noeudsRencontres )

def compression_rec( noeud, noeudsRencontres ):
    if not estFeuille( noeud ):
        noeud.false = compression_rec( noeud.false, noeudsRencontres )
        noeud.true = compression_rec( noeud.true, noeudsRencontres )
    return getDoublon( noeud, noeud.etiquette, noeudsRencontres )

# Parcourt le R-trie noeudRencontres suivant le mot. S'il n'y a rien, y place le noeud. Retourne le noeud à la fin du parcours.
def getDoublon( noeud, etiquette, noeudsRencontres ):
    while len( etiquette ) > 0:
        if not etiquette[ 0 ] in noeudsRencontres:
            noeudsRencontres[ etiquette[ 0 ] ] = dict()
        noeudsRencontres = noeudsRencontres[ etiquette[ 0 ] ]
        etiquette = etiquette[ 1: ]
    if not "noeud" in noeudsRencontres:
        noeudsRencontres[ "noeud" ] = noeud
    return noeudsRencontres[ "noeud" ]

# 3.10
def deletion( arbre ):
    if arbre != None:
        arbre.false = deletion( arbre.false )
        arbre.true = deletion( arbre.true )
        if not estFeuille( arbre ) and arbre.false == arbre.true:
            return arbre.false
        return arbre
    return None

def compression_bdd( arbre ):
    return deletion( compression( arbre ) )