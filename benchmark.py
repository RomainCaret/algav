# encoding: utf-8

from math import floor
from timeit import timeit
from booleens import table
from arbre import cons_arbre, estFeuille
from compression import compression_bdd
from random import randrange, seed
seed( 0 )

def compterNoeuds( arbre, noeudsVisites ):
    if arbre in noeudsVisites:
        return 0
    else:
        noeudsVisites.add( arbre )
        if estFeuille( arbre ):
            return 1
        return 1 + compterNoeuds( arbre.false, noeudsVisites ) + compterNoeuds( arbre.true, noeudsVisites )

# 4.15
def benchmarkAleatoire( n_variables, n_echantillons ):

    taillesArbres = []
    dureeAffichee = 0
    dureeReelle = 0

    for i in range( n_echantillons ):
        if floor( dureeReelle ) - floor( dureeAffichee ) > 0:
            dureeAffichee = dureeReelle
            print( "Progrès :", i / n_echantillons )
            print( "Durée :", dureeReelle )
        booleens = table( randrange( 2 ** ( 2 ** n_variables ) ), 2 ** n_variables )
        ajouter_taille = lambda: taillesArbres.append( compterNoeuds( compression_bdd( cons_arbre( booleens ) ), set() ) )
        dureeReelle += timeit( ajouter_taille, number = 1 )

    fichier = open( "tailles" + str( n_variables ) + ".csv", "w" )
    fichier.write( "Temps (s) :," + str( dureeReelle ) + "\n" )
    fichier.write( "\n".join( map( str, taillesArbres ) ) )
    fichier.close()

def benchmarkExact( n_variables ):

    taillesArbres = []
    dureeAffichee = 0
    dureeReelle = 0
    n_echantillons = 2 ** ( 2 ** n_variables )

    for i in range( n_echantillons ):
        if floor( dureeReelle ) - floor( dureeAffichee ) > 0:
            dureeAffichee = dureeReelle
            print( "Progrès :", i / n_echantillons )
            print( "Durée :", dureeReelle )
        booleens = table( i, 2 ** n_variables )
        ajouter_taille = lambda: taillesArbres.append( compterNoeuds( compression_bdd( cons_arbre( booleens ) ), set() ) )
        dureeReelle += timeit( ajouter_taille, number = 1 )

    fichier = open( "tailles" + str( n_variables ) + ".csv", "w" )
    fichier.write( "Temps (s) :," + str( dureeReelle ) + "\n" )
    fichier.write( "\n".join( map( str, taillesArbres ) ) )
    fichier.close()