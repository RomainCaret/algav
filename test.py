# encoding: utf-8

from random import randrange
from booleens import table
from arbre import cons_arbre, estFeuille
from compression import compression_bdd
from synthese import synthese

def execution( arbre, variables ):
    if estFeuille( arbre ):
        return arbre.cle
    i = int( arbre.cle[ 1: ] )
    return execution( arbre.true if variables[ i - 1 ] else arbre.false, variables )

# 5.19
def test( n_variables, operateur ):
    i1 = randrange( 2 ** ( 2 **( n_variables ) ) )
    i2 = randrange( 2 ** ( 2 **( n_variables ) ) )
    f1 = compression_bdd( cons_arbre( table( i1, 2 ** n_variables ) ) )
    f2 = compression_bdd( cons_arbre( table( i2, 2 ** n_variables ) ) )
    f1_op_f2 = synthese( f1, f2, operateur )
    for i in range( 2 ** n_variables ):
        variables = table( i, n_variables )
        assert operateur( execution( f1, variables ), execution( f2, variables ) ) == execution( f1_op_f2, variables )

def tests( n_variables, operateur, n_echantillons ):
    for _ in range( n_echantillons ):
        test( n_variables, operateur )