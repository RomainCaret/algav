# encoding: utf-8

# 1.2
def decomposition( nombre ):
    l = []
    while ( nombre >= 1 ):
        l += [ nombre % 2 == 1 ]
        nombre = nombre // 2
    return l

# 1.3
def completion( table_booleens, taille_completion ):
    return [
        table_booleens[ i ] if i < len( table_booleens )
        else False
        for i in range( taille_completion )
    ]

# 1.4
def table( nombre, taille_completion ):
    return completion( decomposition( nombre ), taille_completion )